const express = require("express");
const bodyParser = require("body-parser");
const _ = require("lodash");
const mongoose = require("mongoose");
const cors = require('cors')
const jwt = require('jsonwebtoken');

const query = new mongoose.Query();



const app = express();

mongoose.connect('mongodb://localhost:27017/ASAP', { useNewUrlParser: true, useUnifiedTopology: true })

const bcrypt = require('bcrypt');
const { count } = require("console");
const saltRounds = 2;

//Just for production will include in .dotenv
const yourPassword = "someRandomPasswordHere";
const accessTokenSecret = "hiThere"



var userSchema = new mongoose.Schema({
  name: String,
  password: String,
  contactNo: String,
  email: String,
  type: String
})


var eventSchema = new mongoose.Schema({
  name: String,
  location: String,
  date: String,
  description: String,
  volunteersCount: Number,
  volunteers: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
  contactNo: String,
  skillsReq: String

})
var donationSchema = new mongoose.Schema({
  userId: String,
  type: String,
  amount: String,
  location: String
})
var User = mongoose.model('User', userSchema);
var Event = mongoose.model('Event', eventSchema);
var Donation = mongoose.model('Donation', donationSchema)





app.use(cors())
app.use(bodyParser.json());
app.use(express.static("public"));


mongoose.set('useFindAndModify', false);
app.post('/users/login', function (req, res) {
  const userData = {
    password: req.body.password,
    email: req.body.email
  }
  // console.log(req.body)

  bcrypt.hash(req.body.password, 2, (err, hash) => {
    // console.log(req.body);

    User.findOne({ email: userData.email }, function (err2, user) {
      if (!err2) {
        console.log(user)
        if (user != null) {
          bcrypt.compare(userData.password, user.password, function (err, result) {
            // console.log(user.email)
            // console.log(user.password)
            if (result == true) {
              const accessToken = jwt.sign({ id: user._id }, accessTokenSecret);
              res.status(200);
              res.send({
                "authenticated": true,
                accessToken

              })


            } else {
              res.status(401);

              res.send({
                "authenticated": false
              })
            }
          });
        } else {
          res.send({
            "authenticated": false
          })
        }

      } else {
        res.send({
          "authenticated": false
        })
      }


    })
  });

})




app.post('/users', function (req, res) {
  let encryptedPass = "";

  bcrypt.hash(req.body.password, 2, (err, hash) => {
    if (err) {
      res.status(400);
      res.send('Encryption Error')
    } else {

      const userData = {
        name: req.body.name,
        password: hash,
        contactNo: req.body.contactNo,
        email: req.body.email,
        type: req.body.type
      }

      let user = new User(userData);
      user.save(function (err, user) {
        if (err) {
          console.log(err);
        } else {
          delete userData.password;
          res.send(userData)
        }
      })
    }
  });

  // console.log(encryptedPass)


})

// Events
app.post('/events', function (req, res) {
  let postData = {
    name: req.body.name,
    location: req.body.location,
    date: req.body.date,
    description: req.body.description,
    volunteersCount: req.body.volunteersCount,

    contactNo: req.body.contactNo,
    skillsReq: req.body.skillsReq
  }
  let event = new Event(postData);

  event.save(function (err, event) {
    if (err) {
      console.log(err);
    } else {
      res.send(postData)
    }
  })
})

app.get('/events', function (req, res) {
  Event.find({}, function (err, events) {
    if (!err) {
      res.send(events)
    }
  })
})


app.get("/users/details", function (req, res) {
  // console.log(req.headers)
  const authHeader = req.headers.authorization;
  const token = authHeader.split(' ')[1];


  jwt.verify(token, accessTokenSecret, (err, user) => {
    if (err) {
      return res.sendStatus(403);
    }
    // console.log(user)
    const id = user.id
    
    User.findOne({_id:id}, function(err,user2){
      if(!err){
        res.send(
          user2
        )
      }
      else {
        res.sendStatus(403);
      }
    })

  });
})

app.get("/events/:eventId/", function (req, res) {



  Event.findOne({ _id: req.params.eventId }, function (err, event) {
    if (!err) {
      res.send(event)
    }
    else {
      res.set(404);
      res.send("Not found")
    }
  })


});

app.post("/events/:eventId/volunteer", function (req, res) {


  const authHeader = req.headers.authorization;
  const token = authHeader.split(' ')[1];


  jwt.verify(token, accessTokenSecret, (err, user) => {
    if (err) {
      return res.sendStatus(403);
    }
    // console.log(user)
    const id = user.id

    User.findOne({ _id: id }, function (err, user2) {
      if (!err) {
        Event.findOne({ _id: req.params.eventId }, function (err, event) {
          let prevVol = event.volunteers
          prevVol.push(id)
          

          let newOne = {

            name: event.name,
            location: event.location,
            date: event.date,
            description: event.description,
            volunteersCount: event.volunteersCount,
            volunteers: prevVol,
            contactNo: event.contactNo,
            skillsReq: event.skillsReq

          }
          // let newOne = event
          // newOne.volunteersCount= count+1,
          // newOne.volunteers= prevVol,

          Event.findByIdAndUpdate({ _id: req.params.eventId }, newOne, function (err, result) {
            if (!err) {

              res.send("success")
            } else {
              console.log(err)
              res.set(400)
              res.send("Not updated")
            }
          })
        })
      }
      else {
        res.sendStatus(403);
      }
    })

  });


})

app.post("/donate", function (req, res) {

  const authHeader = req.headers.authorization;
  const token = authHeader.split(' ')[1];


  jwt.verify(token, accessTokenSecret, (err, user) => {
    if (err) {
      return res.sendStatus(403);
    }
    // console.log(user)
    const id = user.id

    let donationData = {
      userId: id,

      type: req.body.type,
      amount: req.body.amount,

      location: req.body.location
    }
    let donation = new Donation(donationData);

    donation.save(function (err, event) {
      if (err) {
        console.log(err);
        res.set(400)
        res.send({
          "succes":false
        })

      } else {
        res.send({
          "succes":true
        })
      }
    })


  });
})







app.get("/", function (req, res) {
  res.send("ASAP")
});


app.listen(8000, function () {
  console.log("Server started on port 8000");
})