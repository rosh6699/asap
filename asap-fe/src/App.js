import React, { useEffect } from "react";
import PublicRouter from "./router/PublicRouter";
import VolunteerRouter from "./router/VolunteerRouter";
import { getCurrentUser } from "./redux/actions";
import { useDispatch, useSelector } from "react-redux";

function App() {
  const dispatch = useDispatch();
  const state = useSelector((reduxState) => reduxState);
  const { currentUser } = state;

  useEffect(() => {
    dispatch(getCurrentUser())
      .then((result) => {
        console.log("User token valid", result);
      })
      .catch((exp) => {
        console.log("User token invalid");
      });
  }, []);

  return (
    <div>
      {currentUser && currentUser.data ? <VolunteerRouter /> : <PublicRouter />}
    </div>
  );
}
export default App;
