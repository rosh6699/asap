import React from "react";

function BasicPage({ children }) {
  return (
    <div
      style={{
        maxWidth: "900px",
        margin: "auto",
        width: "98vw",
        marginTop: "50px",
      }}
    >
      {children}
    </div>
  );
}

export default BasicPage;
