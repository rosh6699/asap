import React from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { navigate } from "hookrouter";
import Button from "react-bootstrap/Button";

function NavbarAll({ links, showLogout = false }) {
  return (
    <Navbar bg="light" expand="lg">
      <Navbar.Brand className="cursor-pointer" onClick={() => navigate("/")}>
        Help ASAP
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          {links.map((link) => (
            <Nav.Link
              key={JSON.stringify(link)}
              onClick={() => navigate(link.path)}
            >
              {link.text}
            </Nav.Link>
          ))}
          {showLogout && (
            <Button
              variant="danger"
              onClick={() => {
                localStorage.removeItem("asap_token");
                navigate("/");
                window.location.reload();
              }}
            >
              Logout
            </Button>
          )}

          {/* <NavDropdown title="Dropdown" id="basic-nav-dropdown">
        <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
      </NavDropdown> */}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}

export default NavbarAll;
