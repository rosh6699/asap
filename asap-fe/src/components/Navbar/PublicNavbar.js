import React from "react";
import NavbarAll from "./NavbarAll";

function PublicNavbar() {
  return (
    <div className="">
      <NavbarAll
        links={[
          {
            text: "Home",
            path: "/",
          },
          {
            text: "Register",
            path: "/register",
          },
          {
            text: "Login",
            path: "/login",
          },
        ]}
      />
    </div>
  );
}

export default PublicNavbar;
