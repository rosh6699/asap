import React from "react";
import NavbarAll from "./NavbarAll";

function VolunteerNavbar() {
  return (
    <div className="">
      <NavbarAll
        showLogout={true}
        links={[
          {
            text: "Profile",
            path: "/dashboard",
          },
          {
            text: "Add Events",
            path: "/events/add",
          },
        ]}
      />
    </div>
  );
}

export default VolunteerNavbar;
