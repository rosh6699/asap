import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import BasicPage from "../components/Common/BasicPage";
import { useDispatch } from "react-redux";
import { postAddEvents } from "../redux/actions";
import { navigate } from "hookrouter";

function AddEvents() {
  const dispatch = useDispatch();

  const location = ["Delhi", "Banglore", "Calicut", "Hydrabad"];

  const [form, setForm] = useState({
    name: "",
    location: "",
    date: "",
    volunteersCount: "",
    description: "",
    contactNo: "",
    skillsReq: "",
  });

  function changeHandler(e) {
    const { name, value } = e.target;

    setForm({ ...form, [name]: value });
  }

  function submitHandler(e) {
    e.preventDefault();
    try {
      dispatch(postAddEvents(form)).then((result) => {
        navigate("/dashboard");
      });
    } catch (e) {
      console.log(e);
    }
  }

  return (
    <BasicPage>
      <h1 className="mt-5 mb-12">Add Event</h1>
      <Form onSubmit={submitHandler}>
        <Form.Group controlId="formGroupEmail">
          <Form.Label>Name of Event *</Form.Label>
          <Form.Control
            required
            name="name"
            onChange={changeHandler}
            type="text"
            value={form.name}
            placeholder="The event to make a change"
          />
        </Form.Group>
        <Form.Row className="flex">
          <div className="flex flex-1 flex-column">
            <Form.Group controlId="formBasicPassword">
              <Form.Label>Drop off location</Form.Label>
              <Form.Control
                as="select"
                custom
                value={form.location}
                onChange={changeHandler}
                name="location"
              >
                {location.map((item) => (
                  <option key={item} value={item}>
                    {item}
                  </option>
                ))}
              </Form.Control>
            </Form.Group>
          </div>
          <div className="flex flex-1 flex-column">
            <Form.Group controlId="formGroupPassword">
              <Form.Label>Date *</Form.Label>
              <Form.Control
                required
                name="date"
                onChange={changeHandler}
                type="text"
                value={form.date}
                placeholder="The time of the event"
              />
            </Form.Group>
          </div>
        </Form.Row>
        <Form.Group controlId="exampleForm.Control">
          <Form.Label>Volunteers count *</Form.Label>
          <Form.Control
            name="volunteersCount"
            onChange={changeHandler}
            type="number"
            value={form.volunteersCount}
            required
            placeholder="Number of volunteers needed"
          />
        </Form.Group>
        <Form.Group controlId="exampleForm.ControlTextarea1">
          <Form.Label>Description *</Form.Label>
          <Form.Control
            name="description"
            onChange={changeHandler}
            as="textarea"
            value={form.description}
            rows="3"
            required
            placeholder="Explain the details of the event"
          />
        </Form.Group>
        <Form.Row className="flex">
          <div className="flex flex-1 flex-column">
            <Form.Group controlId="formGroupPassword">
              <Form.Label>Contact Number *</Form.Label>
              <Form.Control
                name="contactNo"
                onChange={changeHandler}
                value={form.contectNo}
                type="text"
                required
                placeholder="Contact number"
              />
            </Form.Group>
          </div>
          <div className="flex flex-1 flex-column">
            <Form.Group controlId="formGroupPassword">
              <Form.Label>Skills</Form.Label>
              <Form.Control
                name="skillsReq"
                onChange={changeHandler}
                type="text"
                value={form.skillsReq}
                placeholder="The skills for event"
              />
            </Form.Group>
          </div>
        </Form.Row>
        <Button variant="primary" type="submit">
          Create
        </Button>
      </Form>
    </BasicPage>
  );
}

export default AddEvents;
