import React from "react";
import Button from "react-bootstrap/Button";
import Image from "react-bootstrap/Image";
import { navigate } from "hookrouter";
import { useDispatch, useSelector } from "react-redux";

function Dashboard() {
  const state = useSelector((reduxState) => reduxState);
  const { currentUser } = state;
  console.log(currentUser);
  const user = currentUser.data;

  return (
    <div className="w-max-screen justify-center py-24 lg:py-48">
      <div className="container mx-auto max-w-sm rounded-lg overflow-hidden shadow-lg my-2 bg-white">
        <div className="relative pb-2/3">
          <img
            className="h-48 w-full object-cover"
            src="https://images.unsplash.com/photo-1549068106-b024baf5062d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=640&h=200"
            alt="Profile image"
          />
        </div>
        <div className="pt-6 pb-8 text-gray-600 text-center">
          <p>{user.name}</p>
          <p className="text-sm">{user.email}</p>
        </div>

        <div className="pb-10 uppercase text-center tracking-wide flex justify-around">
          <div className="posts">
            <p className="text-gray-400 text-sm">Events Participated</p>
            <p className="text-lg font-semibold text-blue-300">76</p>
          </div>
          <div className="followers">
            <p className="text-gray-400 text-sm">Amount Donated</p>
            <p className="text-lg font-semibold text-blue-300">964</p>
          </div>
          <div className="following" className="flex items-center">
            <div>
              <Button onClick={() => navigate("/donate")} variant="primary">
                Make a donation
              </Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Dashboard;
