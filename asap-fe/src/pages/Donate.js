import React from "react";
import Button from "react-bootstrap/Button";
import Image from "react-bootstrap/Image";
import { navigate } from "hookrouter";

function Donate() {
  return (
    <div style={{ margin: "auto", maxWidth: "800px" }}>
      <h1>Make a donation</h1>
      <h3>You are a superhero!</h3>
      <div className="mt-24 flex flex-column px-8">
        <div className="flex align-center justify-center">
          <Image
            src={require("../assets/qr.jpg")}
            style={{ height: "200px", width: "200px" }}
          />
        </div>
        <Button
          onClick={() => {}}
          variant="secondary"
          size="lg"
          className="mt-8 mb-8"
        >
          Cash donation
        </Button>
        <Button
          onClick={() => navigate("/donate/item")}
          variant="secondary"
          size="lg"
          className="mb-12"
        >
          Materials donation
        </Button>
        <p>
          Anything you donation will help the many people go through these hard
          times.
        </p>
      </div>
    </div>
  );
}

export default Donate;
