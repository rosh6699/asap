import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { postItem } from "../redux/actions";
import BasicPage from "../components/Common/BasicPage";
import Alert from "react-bootstrap/Alert";
import { navigate } from "hookrouter";
import { useDispatch } from "react-redux";

function DonateItem() {
  const [errorShow, setErrorShow] = useState(false);
  const items = [
    {
      value: "masks",
      text: "Masks",
      unit: "no",
    },
    {
      value: "sanitizer",
      text: "Sanitizer",
      unit: "bottles",
    },
    {
      value: "books",
      text: "Books",
      unit: "no",
    },
    {
      value: "rice",
      text: "Rice",
      unit: "kg",
    },
  ];

  const location = ["Delhi", "Banglore", "Calicut", "Hydrabad"];
  const [unit, setUnit] = useState(items[0].unit);
  const dispatch = useDispatch();
  const [form, setForm] = useState({
    type: items[0].value,
    amount: 0,
    location: location[0],
  });

  function changeHandler(e) {
    const { name, value } = e.target;

    setForm({ ...form, [name]: value });
  }

  function submitHandler(e) {
    e.preventDefault();
    console.log(form);
    try {
      dispatch(postItem(form)).then((result) => {
        if (result && result.data && result.data.succes) {
          localStorage.setItem("donate", "true");
          navigate("/dashboard");
        } else {
          setErrorShow("Something went wrong. Please try again");
        }
      });
    } catch (e) {
      setErrorShow(e.getMessage);
    }
  }

  function setItem(e) {
    submitHandler(e);
    const it = items.find((i) => i.value === e.target.value);
    setUnit(it.unit);
  }

  let success = false;
  const item = localStorage.getItem("register");
  if (item === "true") {
    localStorage.removeItem("register");
    success = true;
  }

  return (
    <BasicPage>
      <h1>Donate an Item</h1>

      {errorShow && (
        <Alert variant="danger" onClose={() => setErrorShow(false)} dismissible>
          {errorShow}
        </Alert>
      )}
      <Form onSubmit={submitHandler}>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Item type</Form.Label>
          <Form.Control
            as="select"
            custom
            name="type"
            onChange={setItem}
            value={form.tpye}
          >
            {items.map((item) => (
              <option key={item.value} value={item.value}>
                {item.text}
              </option>
            ))}
          </Form.Control>
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Label>Amount</Form.Label>
          <div className="flex align-center">
            <Form.Control
              onChange={changeHandler}
              type="number"
              name="amount"
              required
              placeholder="Number of the items"
              value={form.amount}
            />
            <div className="mx-2">{unit}</div>
          </div>
        </Form.Group>
        <Form.Group controlId="formBasicPassword">
          <Form.Label>Drop off location</Form.Label>
          <Form.Control
            as="select"
            custom
            value={form.location}
            onChange={changeHandler}
            name="location"
          >
            {location.map((item) => (
              <option key={item} value={item}>
                {item}
              </option>
            ))}
          </Form.Control>
        </Form.Group>

        <Button variant="primary" type="submit">
          Submit
        </Button>
      </Form>
    </BasicPage>
  );
}

export default DonateItem;
