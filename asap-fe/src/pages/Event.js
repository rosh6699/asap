import React, { useState, useEffect } from "react";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import ListGroup from "react-bootstrap/ListGroup";
import Form from "react-bootstrap/Form";
import Alert from "react-bootstrap/Alert";
import Image from "react-bootstrap/Image";
import Spinner from "react-bootstrap/Spinner";
import BasicPage from "../components/Common/BasicPage";
import { getEventDetail, addVolunteer } from "../redux/actions";
import { A, navigate } from "hookrouter";
import { useDispatch, useSelector } from "react-redux";

function Event({ id }) {
  const [success, setSuccess] = useState(false);
  const [cnt, setCnt] = useState(0);
  const dispatch = useDispatch();

  const state = useSelector((reduxState) => reduxState);
  const { eventDetail } = state;

  useEffect(() => {
    dispatch(getEventDetail(id)).then((res) => {
      setCnt(res.data.volunteersCount - res.data.volunteers.length);
    });
  }, []);

  function volunteer() {
    try {
      dispatch(addVolunteer(id)).then((result) => {
        setSuccess(true);
        setCnt(cnt - 1);
      });
    } catch (e) {}
  }

  console.log(eventDetail);

  if (!eventDetail || eventDetail.isFetching) {
    return (
      <BasicPage>
        <Spinner animation="grow" size="lg" />
      </BasicPage>
    );
  }

  if (!eventDetail.data) {
    return (
      <BasicPage>
        <div>Something went wrong</div>
      </BasicPage>
    );
  }

  const evt = eventDetail.data;
  return (
    <BasicPage>
      <Card style={{ width: "12 rem" }}>
        <Card.Img
          style={{ width: "100%", height: "400px" }}
          className="object-cover"
          variant="top"
          src="https://images.unsplash.com/photo-1593113702251-272b1bc414a9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1800&q=80"
        />
        {success && (
          <Alert
            variant="success"
            onClose={() => setSuccess(false)}
            dismissible
          >
            Thank you for volunteering
          </Alert>
        )}
        <Card.Body lassName="mt-5">
          <Button
            variant="secondary"
            onClick={() => navigate("/donate")}
            className="mr-2"
          >
            Donate
          </Button>
          <Button variant="primary" onClick={volunteer}>
            Volunteer
          </Button>
          <span> We need {cnt} more people</span>

          <h1 className="mt-5">{evt.name}</h1>

          <Card.Text label="location">Location: {evt.location}</Card.Text>

          <Card.Text label="date">Date: {evt.date}</Card.Text>

          <Card.Text label="description">{evt.description}</Card.Text>

          <Card.Text label="contactNo">
            Contact Number: {evt.contactNo}
          </Card.Text>

          <Card.Text>Skills Required:</Card.Text>
          <ListGroup>
            {evt.skillsReq &&
              evt.skillsReq
                .split(",")
                .map((sk) => <ListGroup.Item key={sk}>{sk}</ListGroup.Item>)}
          </ListGroup>
        </Card.Body>
      </Card>
    </BasicPage>
  );
}

export default Event;
