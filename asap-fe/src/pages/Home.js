import React, { useState, useEffect } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Alert from "react-bootstrap/Alert";
import Image from "react-bootstrap/Image";
import Spinner from "react-bootstrap/Spinner";
import Card from "react-bootstrap/Card";
import BasicPage from "../components/Common/BasicPage";
import { getEvents } from "../redux/actions";
import { A } from "hookrouter";
import { useDispatch, useSelector } from "react-redux";

function Home() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getEvents());
  }, []);

  const state = useSelector((reduxState) => reduxState);
  const { getEventsList } = state;

  console.log(getEventsList);
  const images = [
    "https://images.unsplash.com/photo-1488521787991-ed7bbaae773c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80",
    "https://images.unsplash.com/photo-1541802645635-11f2286a7482?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
    "https://images.unsplash.com/photo-1461354464878-ad92f492a5a0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
    "https://images.unsplash.com/photo-1547082688-9077fe60b8f9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
    "https://images.unsplash.com/photo-1536825919521-ab78da56193b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
  ];

  return (
    <BasicPage>
      <Image
        className="w-full object-cover"
        style={{ height: "400px" }}
        src="https://images.unsplash.com/photo-1469571486292-0ba58a3f068b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"
        fluid
      />
      <Form
        inline
        style={{ position: "relative", top: "-75px" }}
        className="flex align-center justify-center"
      >
        <div className="flex" style={{ maxWidth: "480px", width: "95vw" }}>
          <Form.Control
            type="text"
            placeholder="Enter Your Location"
            className="flex-grow"
          />
          <Button variant="primary">Search</Button>
        </div>
      </Form>
      <div className="flex justify-around flex-wrap">
        {!getEventsList || getEventsList.isFetching ? (
          <Spinner animation="grow" />
        ) : getEventsList.data ? (
          getEventsList.data.length > 0 ? (
            getEventsList.data.map((event) => (
              <Card style={{ width: "18rem", margin: "15px" }} key={event._id}>
                <Card.Img
                  variant="top"
                  src={images[Math.floor(Math.random() * images.length)]}
                />
                <Card.Body>
                  <Card.Title>{event.name}</Card.Title>
                  <Card.Text>At: {event.location}</Card.Text>
                  <A href={`/events/${event._id}`}>
                    <Button variant="info">More info</Button>
                  </A>
                </Card.Body>
              </Card>
            ))
          ) : (
            <div>No events to show</div>
          )
        ) : (
          <Alert variant="danger">Shoething wentwrong please try again</Alert>
        )}
      </div>
    </BasicPage>
  );
}

export default Home;
