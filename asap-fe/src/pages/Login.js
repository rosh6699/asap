import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Alert from "react-bootstrap/Alert";
import BasicPage from "../components/Common/BasicPage";
import { useDispatch } from "react-redux";
import { postLogin } from "../redux/actions";
import { navigate } from "hookrouter";

function Login() {
  const [show, setShow] = useState(true);
  const [errorShow, setErrorShow] = useState(false);

  const dispatch = useDispatch();

  const [form, setForm] = useState({
    email: "",
    password: "",
  });

  function changeHandler(e) {
    const { name, value } = e.target;

    setForm({ ...form, [name]: value });
  }

  function submitHandler(e) {
    e.preventDefault();
    console.log(form);
    try {
      dispatch(postLogin(form)).then((result) => {
        if (result && result.data && result.data.accessToken) {
          localStorage.setItem("asap_token", result.data.accessToken);
          navigate("/dashboard");
          window.location.reload();
        } else {
          setErrorShow("Username or password incorrect");
        }
      });
    } catch (e) {
      setErrorShow(e.getMessage);
    }
  }

  let success = false;
  const item = localStorage.getItem("register");
  if (item === "true") {
    localStorage.removeItem("register");
    success = true;
  }

  return (
    <BasicPage>
      {success && (
        <Alert variant="success" onClose={() => setShow(false)} dismissible>
          Register was a success. Now login to proceed.
        </Alert>
      )}
      {errorShow && (
        <Alert variant="danger" onClose={() => setErrorShow(false)} dismissible>
          {errorShow}
        </Alert>
      )}
      <h1 className="mt-5 mb-12">Login</h1>
      <Form onSubmit={submitHandler}>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            name="email"
            onChange={changeHandler}
            type="email"
            value={form.email}
            required
            placeholder="Enter email"
          />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            onChange={changeHandler}
            type="password"
            name="password"
            value={form.password}
            required
            placeholder="Password"
          />
        </Form.Group>
        <Button variant="primary" type="submit">
          Submit
        </Button>
      </Form>
    </BasicPage>
  );
}

export default Login;
