import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import BasicPage from "../components/Common/BasicPage";
import { useDispatch } from "react-redux";
import { postRegister } from "../redux/actions";
import { navigate } from "hookrouter";

function Register() {
  const dispatch = useDispatch();

  const [form, setForm] = useState({
    name: "",
    contactNo: "",
    email: "",
    password: "",
  });

  function changeHandler(e) {
    const { name, value } = e.target;
    setForm({ ...form, [name]: value });
  }

  function submitHandler(e) {
    e.preventDefault();
    console.log(form);
    try {
      dispatch(postRegister(form)).then((result) => {
        localStorage.setItem("register", "true");
        navigate("/login");
      });
    } catch (e) {
      console.log(e);
    }
  }

  return (
    <BasicPage>
      <h1 className="mt-5 mb-12">Register</h1>
      <Form onSubmit={submitHandler}>
        <Form.Group controlId="formBasicName">
          <Form.Label>Name</Form.Label>
          <Form.Control
            onChange={changeHandler}
            type="name"
            required
            name="name"
            value={form.name}
            placeholder="Full Name"
          />
        </Form.Group>

        <Form.Group controlId="formBasicPhone">
          <Form.Label>Phone</Form.Label>
          <Form.Control
            type="tel"
            required
            name="contactNo"
            value={form.contactNo}
            onChange={changeHandler}
            placeholder="Enter Phone Number"
          />
          <Form.Text className="text-muted"></Form.Text>
        </Form.Group>

        <Form.Group controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            name="email"
            value={form.email}
            required
            onChange={changeHandler}
            type="email"
            placeholder="Enter Email"
          />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            name="password"
            onChange={changeHandler}
            required
            type="password"
            placeholder="Password"
            value={form.password}
          />
          <Form.Text className="text-muted">Enter a strong password</Form.Text>
        </Form.Group>

        <Button variant="primary" type="submit">
          Register
        </Button>
      </Form>
    </BasicPage>
  );
}

export default Register;
