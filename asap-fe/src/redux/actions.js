import { fire } from "./fire";

export const postLogin = (form) => {
  return fire("postLogin", [], form);
};

export const postRegister = (form) => {
  return fire("postRegister", [], form);
};

export const postAddEvents = (form) => {
  return fire("postAddEvents", [], form);
};

export const getCurrentUser = (form) => {
  return fire("currentUser", [], form);
};

export const getEvents = (form) => {
  return fire("getEventsList", [], form);
};

export const postItem = (form) => {
  return fire("postItem", [], form);
};

export const getEventDetail = (id) => {
  return fire("eventDetail", [id]);
};
export const addVolunteer = (id) => {
  return fire("addVolunteer", [id, "volunteer"], {});
};
