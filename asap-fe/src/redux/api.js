export default {
  postLogin: {
    path: "/users/login",
    method: "POST",
    noAuth: true,
  },
  postRegister: {
    path: "/users",
    method: "POST",
    noAuth: true,
  },
  postAddEvents: {
    path: "/events",
    method: "POST",
    noAuth: false,
  },
  currentUser: {
    path: "/users/details",
    method: "GET",
    noAuth: false,
  },
  getEventsList: {
    path: "/events",
    method: "GET",
    noAuth: true,
  },
  postItem: {
    path: "/donate",
    method: "POST",
    noAuth: false,
  },
  eventDetail: {
    path: "/events",
    method: "GET",
    noAuth: true,
  },

  addVolunteer: {
    path: "/events",
    method: "POST",
    noAuth: false,
  },
};
