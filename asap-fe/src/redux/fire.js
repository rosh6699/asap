import axios from "axios";
import api from "./api";

export const actions = {
  FETCH_REQUEST: "FETCH_REQUEST",
  FETCH_REQUEST_SUCCESS: "FETCH_REQUEST_SUCCESS",
  FETCH_REQUEST_ERROR: "FETCH_REQUEST_ERROR",
  SET_DATA: "SET_DATA",
};

export const setStoreData = (key, value) => {
  return {
    type: actions.SET_DATA,
    key,
    value,
  };
};

export const fetchDataRequest = (key) => {
  return {
    type: actions.FETCH_REQUEST,
    key,
  };
};

export const fetchDataRequestError = (key, error) => {
  return {
    type: actions.FETCH_REQUEST_ERROR,
    key,
    error,
  };
};

export const fetchResponseSuccess = (key, data) => {
  return {
    type: actions.FETCH_REQUEST_SUCCESS,
    key,
    data,
  };
};

export const fire = (
  key,
  path = [],
  params = {},
  urlParam = "",
  multipart = false
) => {
  return (dispatch) => {
    dispatch(fetchDataRequest(key));
    return APIRequest(key, path, params, urlParam, multipart)
      .then((response) => {
        dispatch(fetchResponseSuccess(key, response.data));
        return response;
      })
      .catch((error) => {
        dispatch(fetchDataRequestError(key, error));
      });
  };
};

export const APIRequest = (
  key,
  path = [],
  params = {},
  urlParam = "",
  multipart = false
) => {
  // get api url / method
  const request = { ...api[key] };
  if (path.length > 0) {
    request.path += "/" + path.join("/");
  }
  if (request.path[request.path.length - 1] !== "/") {
    request.path = request.path + "/";
  }
  if (request.method === undefined || request.method === "GET") {
    request.method = "GET";
    const qs = Object.keys(params)
      .map((k) => encodeURIComponent(k) + "=" + encodeURIComponent(params[k]))
      .join("&");
    if (qs !== "") {
      request.path += `?${qs}`;
    }
  }
  // set dynamic params in the URL
  if (urlParam) {
    Object.keys(urlParam).forEach((param) => {
      request.path = request.path.replace(`{${param}}`, urlParam[param]);
    });
  }

  // set authorization header in the request header
  const config = {
    baseURL: process.env.REACT_APP_API_URL,
    headers: {},
  };
  if (!request.noAuth && localStorage.getItem("asap_token")) {
    config.headers["Authorization"] =
      "Bearer " + localStorage.getItem("asap_token");
  }

  if (multipart) {
    config.headers[
      "Content-Type"
    ] = `multipart/form-data; boundary=${params._boundary}`;
    // couldn't set cancel token as it is an object
    // params = params.set("cancelToken", isRunning[key].token);
  }

  const axiosApiCall = axios.create(config);

  return axiosApiCall[request.method.toLowerCase()](request.path, params)
    .then((response) => {
      return response;
    })
    .catch((error) => {
      throw error;
    });
};
