import React from "react";
import { useRoutes, useRedirect } from "hookrouter";
import PublicNavbar from "../components/Navbar/PublicNavbar";
import Home from "../pages/Home";
import About from "../pages/About";
import Login from "../pages/Login";
import Register from "../pages/Register";
import Event from "../pages/Event";

const routes = {
  "/": () => <Home />,
  "/about": () => <About />,
  "/register": () => <Register />,
  "/login": () => <Login />,
  "/events/:id": ({ id }) => <Event id={id} />,
};

const PublicRouter = () => {
  useRedirect("/donate", "/login");
  const pages = useRoutes(routes);

  return (
    <div>
      <PublicNavbar />
      {pages}
      {!pages && (
        <div className="flex justify-center py-16">
          Error 404: Page not found
        </div>
      )}
    </div>
  );
};

export default PublicRouter;
