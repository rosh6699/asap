import React from "react";
import { useRoutes } from "hookrouter";
import VolunteerNavbar from "../components/Navbar/VolunteerNavbar";
import Home from "../pages/Home";
import About from "../pages/About";
import AddEvents from "../pages/AddEvents";
import Dashboard from "../pages/Dashboard";
import Donate from "../pages/Donate";
import DonateItem from "../pages/DonateItem";
import Event from "../pages/Event";

const routes = {
  "/": () => <Home />,
  "/events/add": () => <AddEvents />,
  "/events/:id": ({ id }) => <Event id={id} />,
  "/events/:id/donate": ({ id }) => <Donate />,
  "/about": () => <About />,
  "/dashboard": () => <Dashboard />,
  "/donate": () => <Donate />,
  "/donate/item": () => <DonateItem />,
};

const VolunteerRouter = () => {
  const pages = useRoutes(routes);

  return (
    <div>
      <VolunteerNavbar />
      {pages}
      {!pages && (
        <div className="flex justify-center py-16">
          Error 404: Page not found
        </div>
      )}
    </div>
  );
};

export default VolunteerRouter;
